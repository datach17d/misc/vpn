terraform {
  required_providers {
    libvirt = {
      source = "dmacvicar/libvirt"
      version = "0.6.14"
    }
  }
}

provider "libvirt" {
 uri = "qemu:///system"
}

resource "libvirt_volume" "vpn-disk-base" {
    name = "data-volume-vpn-base"
    pool = "default"
    source = "../packer/ubuntu.img"
}

resource "libvirt_volume" "vpn-disk" {
    name = "data-volume-vpn"
    pool = "default"
    base_volume_id = libvirt_volume.vpn-disk-base.id
    size = 20*1024*1024*1024
}

variable "openconnect_server" {
    type = string
}

variable "openconnect_user" {
    type = string
}

variable "openconnect_cookie" {
    type = string
    sensitive = true
}

variable "openconnect_protocol" {
    type = string
    default = "pulse"
}

variable "ssh_pubkey_path" {
    type = string
    default = "~/.ssh/id_rsa.pub"
}

variable "ssh_user" {
    type = string
    default = "ubuntu" 
}

resource "libvirt_cloudinit_disk" "vpn_init" {
  name      = "vpn-commoninit.iso"
  user_data = templatefile("${path.module}/vpn_init.cfg", {
    openconnect_server="${var.openconnect_server}",
    openconnect_crt=indent(6,file("${path.module}/openconnect.crt")),
    openconnect_user="${var.openconnect_user}",
    openconnect_cookie="${var.openconnect_cookie}",
    openconnect_protocol="${var.openconnect_protocol}",
    ssh_pubkey=file("${var.ssh_pubkey_path}"),
    ssh_user="${var.ssh_user}"
  })
}

#data "template_file" "vpn_init" {
#  template = file("${path.module}/vpn_init.cfg")
#}

resource "libvirt_network" "vpn" {
  name = "vpn"
  bridge = "vpn"
  mode = "nat"
  addresses = ["192.168.168.0/29"]
  dns {
    enabled = true
  }
  dhcp {
    enabled = true
  }
}

resource "libvirt_domain" "vpn" {
  name = "vpn"
  autostart = true
  memory = "8192"
  vcpu = 4
  running = true
 
  cloudinit = libvirt_cloudinit_disk.vpn_init.id

  cpu {
    mode = "host-passthrough"
  }

  network_interface {
    network_id = libvirt_network.vpn.id
    addresses = ["192.168.168.2"]
    hostname = "vpn"
  }

  disk {
    volume_id = libvirt_volume.vpn-disk.id
  }

  console {
    type = "tcp"
    target_type = "serial"
    target_port = "0"
    source_service = "22000"
  }

  video {
    type = "virtio"
  }

  xml {
    xslt = <<EOF
    <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
      <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" />

      <xsl:template match="@*|node()">
        <xsl:copy>
          <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
      </xsl:template>

      <xsl:template match="model[@type='virtio']">
        <xsl:copy>
          <xsl:copy-of select="@*"/>
          <xsl:copy-of select="node()"/>
          <acceleration accel3d="yes"/>
        </xsl:copy>
      </xsl:template>

    </xsl:stylesheet>
    EOF
  }

}

