#!/bin/bash -xe

export DEBIAN_FRONTEND=noninteractive

apt-get update
apt-get install -y openconnect \
                   x11-xserver-utils \
                   firefox vim

cloud-init clean
# Remove machine-id for DHCP so that hosts don't get same ip-address
echo -n > /etc/machine-id
userdel --remove --force packer
